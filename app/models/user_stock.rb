class UserStock < ApplicationRecord
  belongs_to :user
  belongs_to :stock
  validates :user_id, :stock_id, presence: true
end
