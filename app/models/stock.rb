# frozen_string_literal: true

# == Schema Information
#
# Table name: stock
#
#  created_at :datetime
#  last_price :decimal
#  name       :string(255)
#  ticker     :string(255)
#  updated_at :datetime
#

class Stock < ApplicationRecord
  has_many :user_stocks
  has_many :users, through: :user_stocks
  validates :name, :ticker, presence: true, uniqueness: { case_sensitive: false }
  validates :ticker, length: { maximum: 6 }

  # class methods
  def self.new_lookup(ticker_symbol)
    client = connect_api_client
    begin
      new(name: client.company(ticker_symbol).company_name, ticker: ticker_symbol.upcase, last_price: client.quote(ticker_symbol).latest_price)
    rescue StandardError => e
      puts e
      nil
    end
  end

  def self.most_active_stocks
    client = connect_api_client
    @most_active_stocks = client.stock_market_list(:mostactive)
  end

  def self.check_db(ticker_symbol)
    find_by(ticker: ticker_symbol)
  end

  def self.update_price(stock)
    client = connect_api_client
    price = client.quote(stock.ticker).latest_price
    stock.update(last_price: price)
  end

  def self.connect_api_client
    IEX::Api::Client.new(publishable_token: Rails.application.credentials.iex_client[:sandbox_api_key], endpoint: 'https://sandbox.iexapis.com/v1')
  end
end
