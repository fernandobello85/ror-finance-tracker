# frozen_string_literal: true

# == Schema Information
#
# Table name: stock
#
#  created_at           :datetime
#  email                :string(255),     null: false
#  encrypted_password   :string(255)      default(""), not null
#  first_name           :string(255)
#  last_name            :string(255)
#  updated_at           :datetime
#

class User < ApplicationRecord
  has_many :user_stocks, foreign_key: :user_id, dependent: :destroy
  has_many :stocks, through: :user_stocks
  has_many :active_friendships, class_name: 'Friendship', foreign_key: 'follower_id', dependent: :destroy
  has_many :passive_friendships, class_name: 'Friendship', foreign_key: 'followed_id', dependent: :destroy
  has_many :followed, through: :active_friendships, source: :followed
  has_many :followers, through: :passive_friendships, source: :follower

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :email, presence: true, uniqueness: { case_sensitive: false }

  def under_stock_limit?
    stocks.count < 10
  end

  def stock_already_tracked?(ticker_symbol)
    stocks.where(ticker: ticker_symbol).any?
  end

  def can_track_stock?(ticker_symbol)
    under_stock_limit? && !stock_already_tracked?(ticker_symbol)
  end

  def full_name
    return "#{first_name} #{last_name}" if first_name || last_name

    'Anonymous'
  end

  def following?(friend)
    followed.include?(friend)
  end

  def followed?(friend)
    followers.include?(friend)
  end

  def except_current_user(friend_matches)
    friend_matches.reject { |user| user.id == self.id }
  end

  # class methods
  def self.search(param)
    param.strip!
    to_send_back = (first_name_matches(param) + last_name_matches(param) + email_matches(param)).uniq
    return nil unless to_send_back

    to_send_back
  end

  def self.first_name_matches(param)
    matches('first_name', param)
  end

  def self.last_name_matches(param)
    matches('last_name', param)
  end

  def self.email_matches(param)
    matches('email', param)
  end

  def self.matches(field_name, param)
    where("#{field_name} like ?", "%#{param}%")
  end
end
