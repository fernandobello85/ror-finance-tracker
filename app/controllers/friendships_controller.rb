class FriendshipsController < ApplicationController

  def create
    followed = User.find(params[:friend])
    return if current_user.following?(followed.id)

    Friendship.create!(follower_id: current_user.id, followed_id: followed.id)
    flash[:notice] = "The friend #{followed.full_name} has been followed successfully"
    redirect_to followed_path
  end

  def destroy
    followed = User.find(params[:id])
    friendship = Friendship.find_by(follower_id: current_user.id, followed_id: followed.id)
    friendship.destroy
    flash[:notice] = "#{followed.full_name} has been unfollowed successfully"
    redirect_to followed_path
  end
end
