class UserStocksController < ApplicationController

  def create
    stock = Stock.check_db(params[:ticker])
    if stock.blank?
      stock = Stock.new_lookup(params[:ticker])
      stock.save!
    end
    UserStock.create!(user: current_user, stock: stock)
    flash[:notice] = "The stock #{stock.name} has been added to your portfolio successfully"
    redirect_to my_portfolio_path
  end

  def destroy
    stock = Stock.find(params[:id])
    user_stock = UserStock.find_by(user_id: current_user.id, stock_id: stock.id)
    user_stock.destroy
    flash[:notice] = "#{stock.name} has been untracked successfully"
    redirect_to my_portfolio_path
  end
end
