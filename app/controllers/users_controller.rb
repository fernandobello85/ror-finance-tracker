class UsersController < ApplicationController
  before_action :find_user, only: [:show, :destroy]

  def my_portfolio
    @user = current_user
    @tracked_stocks = current_user.stocks
  end

  def followed
    @followed = current_user.followed
  end

  def followers
    @followers = current_user.followers
  end

  def show
    redirect_to my_portfolio_path if @user == current_user
    @tracked_stocks = @user.stocks
  end

  def destroy
    @user.destroy
    session[:user_id] = nil if current_user == @user
    respond_to do |format|
      format.html { redirect_to root_url, notice: 'User was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def search
    if params[:friend].present?
      @friend_matches = User.search(params[:friend])
      @friend_matches = current_user.except_current_user(@friend_matches)
      if @friend_matches.any?
        respond_to do |format|
          format.js { render partial: 'users/friend_result' }
        end
      else
        respond_to do |format|
          flash.now[:alert] = "Couldn't find user"
          format.js { render partial: 'users/friend_result' }
        end
      end
    else
      respond_to do |format|
        flash.now[:alert] = 'Please enter a friend name or email to search'
        format.js { render partial: 'users/friend_result' }
      end
    end
  end

  private

  def find_user
    @user = User.find(params[:id])
  end
end
