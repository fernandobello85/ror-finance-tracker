class StocksController < ApplicationController

  def search
    if params[:stock].present?
      @stock = Stock.new_lookup(params[:stock])
      if @stock
        respond_to do |format|
          format.js { render partial: 'users/result' }
        end
      else
        respond_to do |format|
          flash.now[:alert] = 'Please enter a valid symbol to search'
          format.js { render partial: 'users/result' }
        end
      end
    else
      respond_to do |format|
        flash.now[:alert] = 'Please enter a symbol to search'
        format.js { render partial: 'users/result' }
      end
    end
  end

  def most_active
    @most_active_stocks = Stock.most_active_stocks
  end

  def update_price
    @user = current_user
    @tracked_stocks = current_user.stocks
    stock = @tracked_stocks.find(params[:id])
    Stock.update_price(stock)
    respond_to do |format|
      format.js { render partial: 'stocks/list' }
    end
  end
end
