Rails.application.routes.draw do
  resources :user_stocks, only: [:create, :destroy]
  resources :friendships, only: [:create, :destroy]
  resources :users, only: [:show, :destroy], :constraints => { :id => /[0-9|]+/ }
  devise_for :users
  root 'welcome#index'
  get 'my_portfolio', to: 'users#my_portfolio'
  get 'followers', to: 'users#followers'
  get 'followed', to: 'users#followed'
  get 'search_friend', to: 'users#search'
  get 'search_stock', to: 'stocks#search'
  get 'most_active', to: 'stocks#most_active'
  post 'update_price', to: 'stocks#update_price'
end
