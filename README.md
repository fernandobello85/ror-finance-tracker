# README

This is the finance tracker app from the Complete Ruby on Rails Developer course. Using the [IEX Finance API](https://github.com/dblock/iex-ruby-client#get-dividends) and deployed on Heroku as [FinanceTracker](https://ror-financial-tracker.herokuapp.com/)

Technical environment:

* Ruby version 2.6.3
* Rails version 6.0.3.2
* Bootstrap 4.3.1

