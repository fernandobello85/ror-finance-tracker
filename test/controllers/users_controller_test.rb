require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @first_user = User.create(email: 'bob@email.com', first_name: 'Bob', last_name: 'Doe', password: 'password123')
    @second_user = User.create(email: 'alice@email.com', first_name: 'Alice', last_name: 'Martin', password: 'password123')
  end

  test 'should get my_portfolio if user signed up' do
    sign_in(@first_user)
    get my_portfolio_url
    assert_response :success
  end

  test 'not authenticated should get redirect' do
    get my_portfolio_url
    assert_response :redirect
  end

  test 'should get followed' do
    sign_in(@first_user)
    get followed_url
    assert_response :success
  end

  test 'should get followers' do
    sign_in(@first_user)
    get followers_url
    assert_response :success
  end

  test 'should get show of another user' do
    sign_in(@first_user)
    get user_path(@second_user)
    assert_response :success
  end

  test 'should redirect if request show own profile' do
    sign_in(@first_user)
    get user_path(@first_user)
    assert_response :redirect
  end

  test 'should delete profile' do
    sign_in(@first_user)
    assert_difference('User.count', -1) do
      delete user_path(@first_user)
    end
  end
end
