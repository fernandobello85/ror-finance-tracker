require 'test_helper'

class StockTest < ActiveSupport::TestCase
  setup do
    @stock = Stock.new(name: 'Alphabet, Inc.', ticker: 'GOOG', last_price: '1429.9')
  end

  test 'stock should be valid' do
    assert @stock.valid?
  end

  test 'name should be present' do
    @stock.name = ' '
    assert_not @stock.valid?
    @stock.name = 'Alphabet, Inc.'
    assert @stock.valid?
  end

  test 'ticker should be present' do
    @stock.ticker = ' '
    assert_not @stock.valid?
    @stock.ticker = 'GOOG'
    assert @stock.valid?
  end

  test 'name and ticker should be unique' do
    @stock.save!
    second_stock = Stock.new(name: 'Alphabet, Inc.', ticker: 'GOOG', last_price: '1429.9')
    assert_not second_stock.valid?
    second_stock = Stock.new(name: 'Netflix, Inc.', ticker: 'NFLX', last_price: '1300')
    assert second_stock.valid?
  end

  test 'ticker should not be too long' do
    @stock.ticker = 'A' * 10
    assert_not @stock.valid?
  end

  test 'check for an existing stock in DB' do
    assert_not Stock.check_db(@stock.ticker)
    @stock.save!
    assert_equal 'Alphabet, Inc.', Stock.check_db(@stock.ticker).name
  end

  test 'update price' do
    @stock.save!
    Stock.update_price(@stock)
    assert_not_equal '1429.9', @stock.last_price
  end

  test 'retreive from API' do
    stock_api = Stock.new_lookup('AMZN')
    assert_equal 'Amazon.com, Inc.', stock_api.name
  end
end
