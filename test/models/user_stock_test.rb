require 'test_helper'

class UserStockTest < ActiveSupport::TestCase
  setup do
    @first_user = User.create(email: 'bob@email.com', first_name: 'Bob', last_name: 'Doe', password: 'password123')
    @stock = Stock.create(name: 'Alphabet, Inc.', ticker: 'GOOG', last_price: '1429.9')
    @user_stock_relation = UserStock.new(user_id: @first_user.id, stock_id: @stock.id)
  end

  test 'should be valid' do
    assert @user_stock_relation.valid?
  end

  test 'should require a user id' do
    @user_stock_relation.user_id = nil
    assert_not @user_stock_relation.valid?
  end

  test 'should require a stock id' do
    @user_stock_relation.stock_id = nil
    assert_not @user_stock_relation.valid?
  end
end
