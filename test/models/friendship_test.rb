require 'test_helper'

class FriendshipTest < ActiveSupport::TestCase
  setup do
    @first_user = User.create(email: 'bob@email.com', first_name: 'Bob', last_name: 'Doe', password: 'password123')
    @second_user = User.create(email: 'alice@email.com', first_name: 'Alice', last_name: 'Martin', password: 'password123')
    @friendship = Friendship.new(follower_id: @first_user.id, followed_id: @second_user.id)
  end

  test 'should be valid' do
    assert @friendship.valid?
  end

  test 'should require a follower id' do
    @friendship.follower_id = nil
    assert_not @friendship.valid?
  end

  test 'should require a followed id' do
    @friendship.followed_id = nil
    assert_not @friendship.valid?
  end
end
