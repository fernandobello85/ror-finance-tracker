require 'test_helper'

class UserTest < ActiveSupport::TestCase

  setup do
    @first_user = User.new(email: 'bob@email.com', first_name: 'Bob', last_name: 'Doe', password: 'password123')
    @second_user = User.new(email: 'alice@email.com', first_name: 'Alice', last_name: 'Martin', password: 'password123')
    @third_user = User.new(email: 'Tom@email.com', first_name: 'Tom', last_name: 'Smith', password: 'password123')
  end

  test 'user should be valid' do
    assert @first_user.valid?
  end

  test 'should return full name' do
    assert_equal 'Bob Doe', @first_user.full_name
  end

  test 'email must be unique' do
    @first_user.save!
    second_user = User.new(email: 'bob@email.com', password: 'password123')
    assert_not second_user.valid?
  end

  test 'should know if following another user' do
    @first_user.followed << @second_user
    assert @first_user.following?(@second_user)
  end

  test 'should know if is followed by another user' do
    @first_user.followers << @second_user
    assert @first_user.followed?(@second_user)
  end

  test 'can not track a stock if already tracked' do
    @first_user.save!
    stock = Stock.create!(name: 'Alphabet, Inc.', ticker: 'GOOG', last_price: '1429.9')
    @first_user.stocks << Stock.first
    assert @first_user.stock_already_tracked?(stock.ticker)
  end

  test 'retreive list of others users' do
    @first_user.save!
    @second_user.save!
    @third_user.save!
    all_users = User.all
    assert_equal 2, @first_user.except_current_user(all_users).count
  end

  test 'search by email' do
    @first_user.save!
    assert_not_empty User.search(@first_user.email)
  end

  test 'search by first name' do
    @first_user.save!
    assert_not_empty User.search(@first_user.first_name)
  end

  test 'search by last name' do
    @first_user.save!
    assert_not_empty User.search(@first_user.last_name)
  end
end
