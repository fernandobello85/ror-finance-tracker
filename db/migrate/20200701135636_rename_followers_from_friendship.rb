class RenameFollowersFromFriendship < ActiveRecord::Migration[6.0]
  def change
    rename_column :friendships, :user_id, :follower_id
    rename_column :friendships, :friend_id, :followed_id
  end
end
